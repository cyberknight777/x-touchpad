# X-TouchPad

Enable TouchPad touch-as-click for Xorg

## Usage

```shell
$ git clone https://gitlab.com/cyberknight777/X-Touchpad.git
$ makepkg -ci
```

## Acknowledgement

Derived from [here](https://github.com/Trumeet/x-touch-touchpad) for my personal usage

## License

GPL 2.0
